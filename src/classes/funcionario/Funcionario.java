package classes.funcionario;


public abstract class Funcionario {

    private String nome;
    private String cpf;
    private double salario;

    public  abstract double getBonificacao();
    
    public String getNome() {
        return nome;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}