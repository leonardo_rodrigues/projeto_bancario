package classes.json;

import java.io.FileReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonRead {

    private JSONParser parser = null;

    public void LerTudo(String fileName) {

        parser = new JSONParser();

        try (FileReader reader = new FileReader("arquivos/"+ fileName +".json")) {
            
            Object obj = parser.parse(reader);

            JSONArray List = (JSONArray) obj;

            List.forEach(c -> convertObj( (JSONObject) c) );

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private void convertObj(JSONObject conta) {

        JSONObject cObject = (JSONObject) conta.get("conta");

        String nome = (String) cObject.get("nome");
        System.out.println("Nome: " + nome);

        //String saldo = (String) cObject.get("saldo");
        //System.out.println("Saldo:" + saldo);

    }
}