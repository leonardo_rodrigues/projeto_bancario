package classes.funcionario;
import interfaces.*;

/**
 * Gerente
 */
public class Gerente extends Funcionario implements Autentica {

    private int senha;
    private int numeroDeFuncionariosGerenciados;

    public boolean autentica(int senha) {

        if (this.senha == senha) {
            return true;
        } else {
            return false;
        }
    }

    public int getNumeroDeFuncionariosGerenciados() {
        return numeroDeFuncionariosGerenciados;
    }

    public void setSenha(int senha) 
    {
        this.senha = senha;
    }

    @Override
    public double getBonificacao() {
        return getSalario() * 0.15;
    }
}