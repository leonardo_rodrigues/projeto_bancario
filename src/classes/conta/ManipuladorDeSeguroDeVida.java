package classes.conta;

public class ManipuladorDeSeguroDeVida {

  private SeguroDeVida seguroDeVida;

  public void criaSeguro(int nApolice, String titular, double valor) {
    this.seguroDeVida = new SeguroDeVida();
    this.seguroDeVida.setNumeroApolice(nApolice);
    this.seguroDeVida.setTitular(titular);
    this.seguroDeVida.setValor(valor);
  }
}