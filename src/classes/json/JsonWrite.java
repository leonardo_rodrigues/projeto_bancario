package classes.json;

import java.io.File;
import java.io.FileWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.FileReader;
import org.json.simple.parser.JSONParser;

public class JsonWrite {

    private File file = null;
    private JSONArray list = null;
    private JSONObject jsonObject = new JSONObject();
    private String parametro;

    public void Add(String key, String value) {
        jsonObject.put(key, value);
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    } 

    public int Salvar(String fileName) {

        file = new File("arquivos/" + fileName +".json");

        try {
            
            if (!file.exists()) {

                file.createNewFile();
                
                list = new JSONArray();
            }
            else {

                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader("arquivos/" + fileName + ".json"));

                list = (JSONArray) obj;   
            }

            JSONObject coluna = new JSONObject();
            coluna.put(this.parametro, jsonObject);

            list.add(coluna);

            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(list.toJSONString());
            fileWriter.flush();
            fileWriter.close();

            return 0;

        } catch (Exception e) {
            return -1;
        }
    }
}