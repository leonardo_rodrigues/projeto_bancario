package classes.conta;

public abstract class Conta {

	private String titular;
	private int numero;
	private String agencia;
	private double saldo;
	private Data dataAbertura;
	private String cpf;
	private static int qtdContas = 0;
	private int identificador;

	Conta(String titular, String cpf, String agencia, int numero, Data dataAbertura) {

		this.setTitular(titular);
		this.setCpf(cpf);
		this.setNumero(numero);
		this.setAgencia(agencia);
		this.setDataAbertura(dataAbertura);

		qtdContas++;
		identificador = Conta.qtdContas;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	Conta() {
		
		qtdContas++;
		identificador = Conta.qtdContas;
	}
	
	public static int getTotalContas() {
		return Conta.qtdContas;
	}
	
	int getIdentificador() {
		return identificador;
	}
	
	public void saca(double valor) {
		
		if (this.saldo >= valor) {
			
			this.saldo -= valor;
		}
	}
	
	public void deposita(double valor) {
		
		this.saldo += valor;
	}
	
	public double getSaldo() {
		return this.saldo;
	}

	public double calculaRendimento() {
		
		return this.saldo * 0.1;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getDataAbertura() {
		return dataAbertura.dataString();
	}

	public void setDataAbertura(Data dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	
	public String recuperaDadosParaImpressao() {
		
		String dados = "Titular: " + getTitular();
		dados += "\nNumero: " + getNumero();
		dados += "\nAgência: " + getAgencia();
		dados += "\nSaldo: " + getSaldo();
		dados += "\nData de Abertura: " + this.getDataAbertura();
		dados += "\nTipo " + this.getTipo();
		
		return dados;
	}

	public abstract String getTipo();

	public void transfere(double valor, Conta conta) {
		this.saca(valor);
		conta.deposita(valor);
	}
}
