package classes.conta;

public class Data {

	private int dia;
	private int mes;
	private int ano;
	
	public Data(int dia, int mes, int ano) {
		
		if (validaData(dia, mes, ano)) {
			
			this.setDia(dia);
			this.setMes(mes);
			this.setAno(ano);
		}
			
	}	
	
	public String dataString() {
		
		String dados = dia + "/" + mes + "/" + ano; 
		
		return dados;
	}
	
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	private boolean validaData(int dia, int mes, int ano) {
		
		if (dia > 0 && dia < 32 && mes > 0 && mes < 13 && ano > 0) {
			
			if ((mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12 && dia <= 31) || (mes==4 || mes==6 || mes==9 || mes==11) && dia <= 30) {
				
				return true;
			}
			else if (mes==2 && (dia<=29 && ano%4==0 && (ano%100!=0 || ano%400==0)) || dia<=28) {
				
				return true;
			}
		}
		
		return false;
	}
}
