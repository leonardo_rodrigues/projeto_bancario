package classes.funcionario;

public class PessoaFisica {

	private String cpf;

	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		
		if (validaCpf(cpf)) {
			this.cpf = cpf;
		}
		
	}
	
	private boolean validaCpf(String cpf) {
		
		if (cpf.length() == 11) {
			return true;
		}
			
		return false;
	}
	
}
