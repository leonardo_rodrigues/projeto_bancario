package classes.conta;

/**
 * ContaPoupanca
 */
public class ContaPoupanca extends Conta {

    @Override
    public String getTipo() {
        return "Conta Poupança";
    }
}