package classes.conta;

public class ManipuladorDeContas {

  private Conta conta;

  public String criaConta(int tipoConta,String titular, String cpf) {
    
    try {
      this.conta = (tipoConta == 1) ? new ContaCorrente() : new ContaPoupanca();
      this.conta.setTitular(titular);
      this.conta.setCpf(cpf);
      this.conta.setAgencia("1641");

      this.conta.setNumero(123456);
      this.conta.setDataAbertura(new Data(11,12,1999));

      return "Cadastro realizado com sucesso!";
    } catch (Exception e) {
      return e.getMessage();
    }
    
  }
}